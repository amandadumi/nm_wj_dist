{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 Root-Finding Methods\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to numerically find the root(s) of a function, there are several methods that can be employed. Here we will explore the Bisection, Secant, Newton-Raphson, and Halley's methods. To illustrate how these methods work, we will look at the following function: $$f(x)\\ =\\ 0.5 x^3 + 0.25 x -7$$\n",
    "\n",
    "A plot of the function can be seen below. Visually, we can see the root occurs between $2 < x < 3$. \n",
    "\n",
    "For these numerical root finding algorithms, guesses are iteratively improved until the difference between the old guess and the new guess is less than a defined threshold. This threshold is called the convergence threshold. The rate of convergence for each method can be defined as the number of steps required to reach this convergence threshold. Compare the number of steps for each method below for the same convergence threshold. \n",
    "\n",
    "This variable is set below as \"tolerance\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "##########\n",
    "# IMPORTS\n",
    "##########\n",
    "# These are packages needed to perform the math and visualization\n",
    "import numpy as np\n",
    "import IPython\n",
    "from IPython.display import Math\n",
    "import matplotlib.pyplot as plt\n",
    "import sympy as syp\n",
    "x, y, z = syp.symbols('x y z')\n",
    "import rf_helper_functions as pf\n",
    "%matplotlib notebook\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "##########\n",
    "# GLOBAL VARIABLES\n",
    "##########\n",
    "tolerance = 1e-6\n",
    "steps_per_method = dict()\n",
    "##########\n",
    "# FUNCTIONS\n",
    "##########\n",
    "\n",
    "\n",
    "def f(x):\n",
    "    return .5 * x**3 + .25 * x - 7\n",
    "\n",
    "\n",
    "def fprime(x):\n",
    "    return 3 * .5 * x**2 + .25\n",
    "\n",
    "\n",
    "def fdoubleprime(x):\n",
    "    return 2 * 3 * .5 * x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pf.plotf_func(-5, 5, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 Bisection Method\n",
    "\n",
    "The bisection method is a conceptually simple starting point for root finding methods. If a function $f(x)$ changes sign between $a$ and $b$, then it must pass through zero. You may have learned this as the intermediate value theorem. \n",
    "\n",
    "\n",
    "The bisection method begins by taking initial guesses $a$ and $b$ that are on either side of a root. An guess of the root is taken as the midpoint, $c\\ =\\ \\frac{a+b}{2}$. If the sign change occurs between $a$ and $c$, the $c$ replaces $b$ and the next bisection is taken. If the sign change is not between $a$ and $c$, then it must occur between $c$ and $b$, and $c$ will replace the value of $a$.  This method guarantees convergence, but tends to be very slow. Often, this method is used to find a starting guess for more efficient root finding methods.\n",
    "\n",
    "<img src=\"gif/Bisection.gif\" width=\"500\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "##########\n",
    "# FUNCTIONS\n",
    "##########\n",
    "\n",
    "\n",
    "def midpoint(a, b):\n",
    "    c = (a + b) / 2.\n",
    "    return c\n",
    "##########\n",
    "# MAIN CODE\n",
    "##########\n",
    "converged = False\n",
    "num_steps = 0\n",
    "a = 0\n",
    "b = 10\n",
    "while not converged:\n",
    "    c = midpoint(a, b)\n",
    "    if np.sign(f(c)) == np.sign(f(a)):\n",
    "        a = c\n",
    "    else:\n",
    "        b = c\n",
    "    if abs((b - a) / 2.0) < tolerance:\n",
    "        converged = True\n",
    "    num_steps += 1\n",
    "method = \"Bisection\"\n",
    "steps_per_method[method] = num_steps\n",
    "print(\"Method: {}\".format(method))\n",
    "print(\"Root = {:.5f}\".format(c))\n",
    "print(\"Number of steps = {}\".format(num_steps))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 Secant Method\n",
    "\n",
    "The Secant method evaluates $f(x)$ with an initial guess at two points ($x_0$ and $x_1$) near the root. To determine the two points of the next iteration, we first find the root ($x_2$) of the secant line between $x_0$ and $x_1$. We can define our secant line from the equation below:\n",
    "$$ y\\ =\\ \\frac{f(x_1)\\ -\\ f(x_0)}{x_1\\ -\\ x_0}(x_2-x_1)\\ +\\ f(x_1)$$\n",
    "\n",
    "To find the root of the secant line, we set $y=0$:\n",
    "$$ 0\\ =\\ \\frac{f(x_1)\\ -\\ f(x_0)}{x_1\\ -\\ x_0}(x_2\\ -\\ x_1)\\ +\\ f(x_1)$$\n",
    "\n",
    "and rearrange to solve for $x_2$. The result is:\n",
    "\n",
    "$$x_2\\ =\\ x_1\\ -\\ f(x_1)\\frac{x_1\\ -\\ x_0}{f(x_1)\\ -\\ f(x_0)}$$\n",
    "\n",
    "The oldest guess ($x_0$) is discarded and the process is repeated with $x_1$ and $x_2$.\n",
    "\n",
    "This is iterated, which results in the following recurrence relation:\n",
    "\n",
    "$$x_n = x_{n-1}\\ -\\ f(x_{n-1})\\frac{x_{n-1}\\ -\\ x_{n-2}}{f(x_{n-1})\\ -\\ f(x_{n-2})}$$\n",
    "\n",
    "<img src=\"gif/secant.gif\" width=\"500\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "##########\n",
    "# MAIN CODE\n",
    "##########\n",
    "x_old = 8\n",
    "x_new = 7\n",
    "num_steps = 0\n",
    "converged = False\n",
    "while not converged:\n",
    "    temp = x_new\n",
    "    x_new = x_new - (f(x_new) * ((x_new - x_old) / (f(x_new) - f(x_old))))\n",
    "    x_old = temp\n",
    "    num_steps += 1\n",
    "    if np.abs(x_new - x_old) < tolerance:\n",
    "        converged = True\n",
    "method = \"Secant\"\n",
    "steps_per_method[method] = num_steps\n",
    "print(\"Method: {}\".format(method))\n",
    "print(\"Root = {:.5f}\".format(x_new))\n",
    "print(\"Number of steps = {}\".format(num_steps))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.3 Netwon-Raphson Method\n",
    "\n",
    "The Newton-Raphson method utilizes a tangent line rather than the secant line we saw previously. This requires the first derivative of our function $f'(x)$. \n",
    "\n",
    "The method begins by taking an initial guess $x_0$.\n",
    "\n",
    "To find a subsequent guess ($x_1$), we then use the following equation of a line tangent to $f(x)$ at $x_0$: \n",
    "$$y\\ =\\ f'(x_0)(x\\ -\\ x_0)\\ +\\ f(x_0)$$\n",
    "\n",
    "The subsequent guess is defined as the root of the tangent line. To find that root we set $y$ equal to $0$:\n",
    "\n",
    "$$0\\ =\\ f'(x_0)(x_1\\ -\\ x_0)\\ +\\ f(x_0)$$\n",
    "\n",
    "This can be rearranged to:\n",
    "\n",
    "$$x_1\\ =\\ x_0\\ -\\ \\frac{f(x_0)}{f'(x_0)}$$\n",
    "\n",
    "This is iterated, which results in the following recurrence relation:\n",
    "\n",
    "$$x_n\\ =\\ x_{n-1}\\ -\\ \\frac{f(x_{n-1})}{f'(x_{n-1})}$$\n",
    "\n",
    "Below is a diagram that illustrates the iterative Newton-Raphson method.\n",
    "<img src=\"gif/Newton.gif\" width=\"500\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "##########\n",
    "# MAIN CODE\n",
    "##########\n",
    "converged = False\n",
    "x_new = 7\n",
    "num_steps = 0\n",
    "while not converged:\n",
    "    x_old = x_new\n",
    "    x_new = x_old - (f(x_old) / (fprime(x_old)))\n",
    "    num_steps += 1\n",
    "    if np.abs(x_new - x_old) < tolerance:\n",
    "        converged = True\n",
    "\n",
    "method = \"Newton-Raphson\"\n",
    "steps_per_method[method] = num_steps\n",
    "print(\"Method: {}\".format(method))\n",
    "print(\"Root = {:.5f}\".format(x_new))\n",
    "print(\"Number Steps = {}\".format(num_steps))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.4 Halley's Method\n",
    "\n",
    "When evaluating the root of a function, methods that incorporate a derivative of the function are known as Householder methods.  For instance, the Newton-Raphson method (discussed above) utilizes the first derivative of the function and is known as a 1st class Householder method.  Second in the class of Householder methods is Halley's method, which incorporates the second derivative of the function. \n",
    "\n",
    "Halley's formula is very similar to the Newton-Raphson, but incorporates the second derivative in the denominator.  This correction is useful if it can be calculated easily. Due to the absence of a known second-derivative in many relevant physical chemistry problems, this method is often only applied for theoretical problems. If unable to be calculated with ease, it is more useful to carry out additional steps of Newton-Raphson.\n",
    "\n",
    "To obtain Halley's relationship, let's first consider the function:\n",
    "\n",
    "$$g(x)\\ =\\ \\frac{f(x)}{\\sqrt{\\left|\\ f'(x)\\ \\right|}}\\ =\\ f(x)\\ \\left|\\ f'(x)\\ \\right|^{-\\frac{1}{2}}$$\n",
    "\n",
    "Now, taking the first derivative of g(x):\n",
    "\n",
    "$$g'(x)\\ =\\ f'(x)\\sqrt{\\left|\\ f'(x)\\ \\right|}\\ -\\ \\frac{f(x)f''(x)}{{2\\sqrt{\\left|\\ f'(x)\\ \\right|}\\ f'(x)}}$$\n",
    "\n",
    "This leads to:\n",
    "\n",
    "$$g'(x)\\ =\\ \\frac{1}{2\\sqrt{\\left|\\ f'(x)\\ \\right|}\\ f'(x)} \\left(2f'(x)^2\\ -\\ f(x)f''(x)\\right)$$\n",
    "\n",
    "which can be simplified to:\n",
    "\n",
    "$$g'(x)\\ =\\ \\frac{2f'(x)^2\\ -\\ f(x)f''(x)}{2f'(x)\\sqrt{\\left|\\ f'(x)\\ \\right|}}$$\n",
    "\n",
    "Now, applying Newton's method:\n",
    "\n",
    "$$x_n\\ =\\ x_{n-1}\\ -\\ \\frac{g(x)}{g'(x)}$$\n",
    "\n",
    "Substituting the $g(x)$ and $g'(x)$ from above and simplifying gives rise to the sequence of iterations for Halley's method:\n",
    "\n",
    "$$x_n\\ =\\ x_{n-1}\\ -\\ \\frac{2f(x_{n-1}) f'(x_{n-1})}{2\\ [\\ f'(x_{n-1})\\ ]^2\\ -\\ f(x_{n-1})f''(x_{n-1})}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The plot below shows f(x) in black, and g(x) in red. We take the\n",
    "\n",
    "<img src=\"gif/Halley.gif\" width=\"500\"/>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "##########\n",
    "# MAIN CODE\n",
    "##########\n",
    "x_new = 7\n",
    "num_steps = 0\n",
    "converged = False\n",
    "while not converged:\n",
    "    x_old = x_new\n",
    "    x_new = x_old - (2 * f(x_old) * fprime(x_old)) / \\\n",
    "        ((2 * fprime(x_old)**2) - (f(x_old) * (fdoubleprime(x_old))))\n",
    "    num_steps += 1\n",
    "    if (np.abs(x_new - x_old) < tolerance):\n",
    "        converged = True\n",
    "method = \"Halley's\"\n",
    "steps_per_method[method] = num_steps\n",
    "print(\"Method: {}\".format(method))\n",
    "print(\"Root = {:.5f}\".format(x_new))\n",
    "print(\"Number of steps = {}\".format(num_steps))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Halley's method is not widely implemented due to the necessity of calculating the second derivative. If a second derivative is avalable, Halley's method is able to out preform Newton's method within a certain number of steps. Below we show a comparison of Newton's method and Halley's method. After beginning at the same starting guess of the root, Halley's method is able to get much closer to the root within the four steps shown.\n",
    "\n",
    "<img src=\"gif/combined.gif\" width=\"800\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3.5 What method should be chosen?\n",
    "Choosing from the many methods that exist for numerical root finding may be overwhelming.\n",
    "\n",
    "First, it is important to consider the number of steps required for convergence. Below the number of steps required to reach the defined convergence threshold are tabulated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"{:->28}\".format(\"\"))\n",
    "print(\"{:^20} : {:>5}\".format(\"Method\", \"Steps\"))\n",
    "print(\"{:-^20}   {:->5}\".format(\"\", \"\"))\n",
    "for i in sorted(steps_per_method, key=steps_per_method.get, reverse=True):\n",
    "    print(\"{:^20} : {:>5}\".format(i, steps_per_method[i]))\n",
    "print(\"{:->28}\".format(\"\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the function is inexpensive to evaluate and the first and second derivatives can be evaluated analytically, we would obviously choose Halley's Method. However, if the function and its derivatives are expensive to evaluate then Newton-Raphson may be preferable as it does not require second derivatives. The secant and bisection methods do not require derivatives, which would be preferable if analytical derivatives are not available. The bisection method has guaranteed convergence, but converges slowly. The choice of method is usually a trade off between convergence time and computational cost."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nodes of 3s orbital\n",
    "\n",
    "The nodes of an s-orbital are places where there is zero probability of finding the electron. These nodes of the s orbital can be found using root finding methods....\n",
    "\n",
    "$$ R_{30}\\ =\\ 2 \\left( \\frac{Z}{3a_0} \\right)^{3/2} \\left( 1 - \\frac{2Zr}{3a_0} +\\frac{2(Zr)^2}{27a_0^2} \\right) e^{-Zr/3a_0}$$\n",
    "\n",
    "# Your task!\n",
    "1. Define a function called `R_30()` which calculates the function above. \n",
    "2. Ensure your function takes three input arguments in the following order: $Z$, $a0$, and $r$. \n",
    "3. Don't forget to have a replace the question marks below with the appropriate value to return!\n",
    "2. Test your function with the second code block below. \n",
    "\n",
    "Hints:\n",
    "1. Remember that when coding an expression such as $2Zr$ to multiply $2$ by $Z$ by $r$, we must write `2*Z*r`\n",
    "2. We learned in the Introduction notebook that for some math function rely on external libraries. Here to code $e^{stuff}$, we will write this as `np.exp(stuff)`. In other words, we represent $e$ with `np.exp()` and any terms we needed exponentiated will go in the parentheses. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "## code your function here\n",
    "def R_30():\n",
    "    return ??"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Z = 6\n",
    "a0 = 1\n",
    "r = 5\n",
    "\n",
    "pf.check_R30(R_30,Z,a0,r)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have correctly coded the example function above, we can find roots of this s orbital function. The nodal position will shift in position as the charge of the nuclei changes. We are going to use the bisection method to track this down. We are using this method for two reasons: it does not require a first derivative and it is guarunteed to converge (although it may take many iterations.)\n",
    "\n",
    "One things we will experience here is how the starting guess in root finding will affect the estimate we get out. \n",
    "\n",
    "# Your task! (continuted)\n",
    "To complete this task, you will run the cell block below **without changing anything**.\n",
    "Continue to the text block below for furhter instruction\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.widgets import Slider, Button\n",
    "import scipy.constants as sc\n",
    "\n",
    "\n",
    "def bisection(a,b,f,Z,a0):\n",
    "    tolerance = 0.000001\n",
    "    converged = False\n",
    "    num_steps = 0\n",
    "    while not converged:\n",
    "        c = midpoint(a,b)\n",
    "        if np.sign(f(Z,a0,c)) == np.sign(f(Z,a0,a)):\n",
    "            a = c\n",
    "        else:\n",
    "            b = c\n",
    "        if abs((b - a) / 2.0) < tolerance:\n",
    "            converged = True\n",
    "        num_steps += 1\n",
    "    return c\n",
    "\n",
    "distance = np.linspace(-0.3,4,100)\n",
    "initial_root_guess1 = 0\n",
    "initial_root_guess2 = .8\n",
    "\n",
    "initial_Z = 6 \n",
    "# calculate R_30 function\n",
    "R = R_30(initial_Z,a0,distance)\n",
    "# calculate the real root with the root_guess at the starting point\n",
    "root_real = bisection(initial_root_guess1,initial_root_guess2,R_30,initial_Z,a0)\n",
    "#Plotting begins here.\n",
    "fig,ax = plt.subplots(figsize=(8,4))\n",
    "plt.grid(True)\n",
    "plt.subplots_adjust(bottom=0.30)\n",
    "\n",
    "ax.set_autoscale_on(True)\n",
    "ax.autoscale_view(True,True,True)\n",
    "\n",
    "line, = ax.plot(distance, R ,label = '$R_{30}$')\n",
    "root_g1, = ax.plot(initial_root_guess1,R_30(initial_Z,a0,initial_root_guess1), linestyle='',marker='.', markersize = 20, color = 'gray')\n",
    "root_g2, = ax.plot(initial_root_guess2,R_30(initial_Z,a0,initial_root_guess2), linestyle='',marker='.', markersize = 20, color = 'gray')\n",
    "root_r, = ax.plot(root_real,R_30(initial_Z,a0,root_real), linestyle='',marker='.',color = 'red')\n",
    "\n",
    "root_annotation = ax.annotate(r'root = {:.2f}'.format(root_real), xy = (max(distance)*0.7,max(R)*0.6))\n",
    "f_annotation = ax.annotate(r\"$f(root)$ = {:.2f}\".format(R_30(initial_Z,a0,root_real)), xy = (max(distance)*0.7,0.5*max(R)))\n",
    "\n",
    "########\n",
    "plt.legend()\n",
    "ax_Z = plt.axes([0.25, 0.1, 0.65, 0.03])\n",
    "axguess = plt.axes([0.25, 0.15, 0.65, 0.03])\n",
    "axguess2 = plt.axes([0.25, 0.20, 0.65, 0.03])\n",
    "s_Z= Slider(ax_Z, 'Z', 1, 40, valinit=initial_Z)\n",
    "s_guess2 = Slider(axguess2, 'root 2 guess', min(distance),max(distance),valinit=initial_root_guess2)\n",
    "s_guess = Slider(axguess, 'root 1 guess', min(distance),max(distance),valinit=initial_root_guess1)\n",
    "\n",
    "def update(val):\n",
    "    #get the new values from sliders\n",
    "    Z = s_Z.val\n",
    "    root_guess1 = s_guess.val\n",
    "    root_guess2 = s_guess2.val\n",
    "    root_real = bisection(root_guess1,root_guess2,R_30, Z,a0)\n",
    "    distance = np.linspace(-0.3,4,100)\n",
    "    R = R_30(Z,a0,distance)\n",
    "    line.set_ydata(R)\n",
    "    root_r.set_data(root_real,R_30(Z,a0,root_real))\n",
    "    root_g1.set_data(root_guess1,R_30(Z,a0,root_guess1))\n",
    "    root_g2.set_data(root_guess2,R_30(Z,a0,root_guess2))\n",
    "    root_annotation.set_text(r'root = {:.2f}'.format(root_real))\n",
    "    f_annotation.set_text(r\"$f(root)$ = {:.2f}\".format(R_30(Z,a0,root_real)))\n",
    "    axes.relim()\n",
    "    \n",
    "    axes.autoscale_view(True,True,True)\n",
    "    fig.canvas.draw_idle()\n",
    "\n",
    "s_Z.on_changed(update)\n",
    "s_guess.on_changed(update)\n",
    "s_guess2.on_changed(update)\n",
    "\n",
    "resetax = plt.axes([0.8, 0.025, 0.1, 0.04])\n",
    "reset_button = Button(resetax, 'Reset', hovercolor='0.975')\n",
    "\n",
    "\n",
    " \n",
    "def reset(event):\n",
    "    s_Z.reset()\n",
    "    s_guess.reset()\n",
    "    s_guess2.reset()\n",
    "    \n",
    "reset_button.on_clicked(reset)\n",
    "\n",
    "\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "For each value of Z, there will be two nodes in the 3-s wavefunction. We are going to locate these two roots as Z changes using the bisection method defined above. \n",
    "\n",
    "# Your Task (continued)\n",
    "1. Move the Z value to be close to 12\n",
    "2. Move the root 2 guess to somewhere left of the root.\n",
    "3. Move the root 1 guess to somewhere right of the root.\n",
    "4. Record where your Z value and  the value of the two roots you find.\n",
    "5. Repeat steps 1-4 for Z values = 3,7, 20, 35\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
